let plateau =
    [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0]];

let hints =
    [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0]];

let doc_plateau = document.getElementById("plateau");
let increment = 36;
//Initialisation du plateau
for (let i = 9; i >= 0; i--) {

    let contain_row = doc_plateau.appendChild(document.createElement("div"));
    contain_row.className += "contain_row";

    let row = contain_row.appendChild(document.createElement("div"));
    row.className += "row";

    let hint = contain_row.appendChild(document.createElement("div"));
    hint.className += "hint";

    for (let j = 0; j < 4; j++) {
        let img = row.appendChild(document.createElement("img"));
        img.className += "margin";
        img.id = "i_" + increment;
        img.src = "7.png";

        let img_hint = hint.appendChild(document.createElement("img"));
        img_hint.height = 20;
        img_hint.width = 20;
        img_hint.id = "h_" + increment;
        img_hint.src = "7.png";

        plateau[i][j] = document.getElementById(img.id);
        hints[i][j] = document.getElementById(img_hint.id);

        increment++;
    }
    increment -= 8;
}

//Game
let mind = [alea(), alea(), alea(), alea()];

/**
 *
 * @returns number n
 */
function alea() {
    let bMin = 1;
    let bMax = 5;

    let n = Math.random() * (bMax - bMin) + bMin;
    n = Math.round(n);
    return n;
}

let finish = false;

let currents = [0,0,0,0];
let row = 0;
let click = 0;

function addJeton(image) {
    if(finish) return;

    let p_doc = plateau[row][click];
    p_doc.src = image.src;
    currents[click] = foundColor(p_doc);

    click++;

    if (click >= 4) {
        click = 0;

        finish = hasWin();
        if (finish && row < 10) {
            let mind_ = document.getElementById('alea_1');
            mind_.src = mind[0] + ".png";
            mind_ = document.getElementById('alea_2');
            mind_.src = mind[1] + ".png";
            mind_ = document.getElementById('alea_3');
            mind_.src = mind[2] + ".png";
            mind_ = document.getElementById('alea_4');
            mind_.src = mind[3] + ".png";

            doc_plateau.innerHTML += "VOUS AVEZ GAGNÉ";
        } else if(!finish && row >= 9) {
            let mind_ = document.getElementById('alea_1');
            mind_.src = mind[0] + ".png";
            mind_ = document.getElementById('alea_2');
            mind_.src = mind[1] + ".png";
            mind_ = document.getElementById('alea_3');
            mind_.src = mind[2] + ".png";
            mind_ = document.getElementById('alea_4');
            mind_.src = mind[3] + ".png";

            doc_plateau.innerHTML += "VOUS AVEZ PERDU";
        }
        currents = [0,0,0,0];

        row++;
    }

}

let win = 0;
function hasWin(){

    for(let j = 0; j < 4; j++){
        let p_n = mind[j];
        let h_n = currents[j];

        if(inArray(h_n)){
            if(p_n === h_n){
                hints[row][j].src = "8.png";
                win++;
            } else {
                hints[row][j].src = "9.png";
            }
        }
    }

    if(win >= 4){
        return true;
    } else {
        win = 0;
        return false;
    }

}

function inArray(h_n){

    let b = false;

    for(let j = 0; j < 4; j++) {
        let p_n = mind[j];

        if(p_n === h_n){
            b = true;
            return b;
        }

    }

    return b;
}

/**
 *
 * @param doc
 * @returns number num
 */
function foundColor(doc) {
    let _src = doc.src;
    let num = parseInt(_src.substr(_src.length - 5, _src.length));
    return num;
}

